# SnapScrap.py
Downloads public Snapchat stories in your System.

Creates a folder in the script directory with the name of a given Snapchat username.

## Saving this locally
```bash
$ git clone https://codeberg.org/allendema/SnapScrap.py.git
$ pip3 install -r requirements.txt
```

## Run it
```bash
$ cd SnapScrap.py # Changing to this directory.
$ chmod +x SnapScrap.py # Make the script executable.
$ ./SnapScrap.py # Running the script.
```
## Use it
Just enter one username which has Public Profile.
It saves the media in a Folder with the name of given username. 
## Heads Up
Made by exploring Python. Inspired by similiar programms. Use at own risk.

## Use it to archive important things, be polite and cause no harm.

## Bonus Information
Depending on changes Snap makes, they might or might not block your IP.
Has not happend 'till now.

[Apache License 2.0](https://codeberg.org/allendema/SnapScrap.py/src/branch/main/LICENSE)

Now on [Codeberg](https://codeberg.org/explore/repos) too!

Allen 2021
